package com.tdp.vass.mstdpclients;

import static org.assertj.core.api.Assertions.assertThat;

import com.tdp.vass.mstdpclients.controller.ClientController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MsTdpClientsApplicationTests {

	@Autowired
	private ClientController clientController;

	@Test
	void contextLoads() {
		assertThat(clientController).isNotNull();
	}

}
