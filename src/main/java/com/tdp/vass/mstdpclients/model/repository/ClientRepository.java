package com.tdp.vass.mstdpclients.model.repository;

import com.tdp.vass.mstdpclients.model.entity.Clients;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Clients, Long> {

    Clients findByNumDoc(String numDoc);

}
