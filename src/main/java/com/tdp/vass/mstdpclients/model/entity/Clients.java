package com.tdp.vass.mstdpclients.model.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@ApiModel("Model Client")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "clients")
public class Clients {

    @Id
    @ApiModelProperty(value = "Id de cliente", required = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "Tipo de documento", required = true)
    @Column(name = "doc_type")
    private String docType;

    @ApiModelProperty(value = "Numero de documento", required = true)
    @Column(name = "num_doc")
    private String numDoc;

    @ApiModelProperty(value = "Nombre de cliente", required = true)
    @Column(name = "first_name")
    private String firstName;

    @ApiModelProperty(value = "Apellidos de cliente", required = true)
    @Column(name = "last_name")
    private String lastName;

    @ApiModelProperty(value = "Fecha de nacimiento de cliente", required = true)
    @Column(name = "birth_date")
    private Date birthDate;

    @JoinTable(
            name = "client_services",
            joinColumns = @JoinColumn(name = "id_client", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "id_services", nullable = false)
    )
    @ManyToMany(cascade = CascadeType.ALL)
    private List<MovServices> services;

}
