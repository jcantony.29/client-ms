package com.tdp.vass.mstdpclients.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@ApiModel("Model Services")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "mov_services")
public class MovServices {


    @Id
    @ApiModelProperty(value = "Id de servicio", required = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "codigo de Servicio", required = true)
    private String code;

    @ApiModelProperty(value = "Descripcion de servicio", required = true)
    private String description;

    @ApiModelProperty(value = "Tecnologia del Servicio", required = true)
    private String technology;
}
