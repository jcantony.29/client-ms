package com.tdp.vass.mstdpclients.controller;

import com.tdp.vass.mstdpclients.dto.ResponseApi;
import com.tdp.vass.mstdpclients.service.ClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(value = "Endpoint de Clientes", consumes = "endpoitn Que Permite consultar clientes")
@RestController
@RequestMapping("/v1")
public class ClientController {

    private ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @ApiOperation(value = "Busca clientes", notes = "endpoint para obtener clientes")
    @GetMapping("/")
    public ResponseEntity<?> findAllClients() {
        try {
            return ResponseEntity.ok(new ResponseApi(200, "Success", clientService.getClient()));
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new ResponseApi(500, "Error: " + e.getMessage()));
        }
    }


    @ApiOperation(value = "Busca cliente por documento", notes = "endpoint para obtener clientes")
    @GetMapping("/{numDoc}")
    public ResponseEntity<?> findByNumDoc(@PathVariable String numDoc) {
        try {
            return ResponseEntity.ok(new ResponseApi(200, "Success", clientService.findByDocument(numDoc)));
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new ResponseApi(500, "Error: " + e.getMessage()));
        }
    }


}
