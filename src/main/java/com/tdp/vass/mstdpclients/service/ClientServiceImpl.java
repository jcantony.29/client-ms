package com.tdp.vass.mstdpclients.service;

import com.tdp.vass.mstdpclients.model.entity.Clients;
import com.tdp.vass.mstdpclients.model.repository.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public List<Clients> getClient() {
        Iterable<Clients> clientsIterable = clientRepository.findAll();
        List<Clients> clientList = new ArrayList<>(1);
        clientsIterable.forEach(clientList::add);
        return clientList;
    }

    @Override
    public Clients findByDocument(String docNumber) {
        return clientRepository.findByNumDoc(docNumber);
    }
}
