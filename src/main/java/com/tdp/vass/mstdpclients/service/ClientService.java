package com.tdp.vass.mstdpclients.service;

import com.tdp.vass.mstdpclients.model.entity.Clients;
import java.util.List;

public interface ClientService {

    List<Clients> getClient();

    Clients findByDocument(String docNumber);

}
