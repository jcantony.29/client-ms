package com.tdp.vass.mstdpclients;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsTdpClientsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsTdpClientsApplication.class, args);
    }

}
